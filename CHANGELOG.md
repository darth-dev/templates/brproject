# Changelog

Todas as mudanças significativas neste projeto serão documentadas neste arquivo. Isso inclui entrevistas com o cliente, mudanças nos requisitos, problemas enfrentados, entre outros. No entanto, é importante usar este registro com cautela, evitando detalhamentos extensivos de funcionalidades e requisitos, que devem ser registrados nas **issues**.

O formato é baseado no [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## 2022-04-22
### Added
- Added a new token type `text_special` to store escaped characters, same as `text` but
  unaffected by replacement plugins (smartquotes, typographer, linkifier, etc.).
- Added a new rule `text_join` in `core` ruler. Text replacement plugins may choose to
  insert themselves before it.

### Changed
- `(p)` is no longer replaced with § by typographer (conflicts with ℗), #763.
- `text_collapse` rule is renamed to `fragments_join`.

### Fixed
- Smartquotes, typographic replacements and plain text links can now be escaped
  with backslash (e.g. `\(c)` or `google\.com` are no longer replaced).
- Fixed collision of emphasis and linkifier (so `http://example.org/foo._bar_-_baz`
  is now a single link, not emphasized). Emails and fuzzy links are not affected by this.


## 2022-03-19
### Changed
- Updated CM spec compatibility to 0.31.2, #1009.

### Fixed
- Fixed quadratic complexity when parsing references, #996.
- Fixed quadratic output size with pathological user input in tables, #1000.


## 2022-02-22
### Added
- Added a new token type `text_special` to store escaped characters, same as `text` but
  unaffected by replacement plugins (smartquotes, typographer, linkifier, etc.).
- Added a new rule `text_join` in `core` ruler. Text replacement plugins may choose to
  insert themselves before it.

### Changed
- `(p)` is no longer replaced with § by typographer (conflicts with ℗), #763.
- `text_collapse` rule is renamed to `fragments_join`.

### Fixed
- Smartquotes, typographic replacements and plain text links can now be escaped
  with backslash (e.g. `\(c)` or `google\.com` are no longer replaced).
- Fixed collision of emphasis and linkifier (so `http://example.org/foo._bar_-_baz`
  is now a single link, not emphasized). Emails and fuzzy links are not affected by this.


## 2022-01-08
### Security
- Fix possible ReDOS in newline rule. Thanks to @MakeNowJust.
